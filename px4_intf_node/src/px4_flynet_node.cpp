#include <ros/ros.h>
#include <mavros/mavros.h>
#include <mavros_msgs/SetMode.h>
#include <mavros_msgs/CommandBool.h>
#include <mavros_msgs/State.h>
#include <mavros_msgs/CommandHome.h>
#include <mavros_msgs/CommandTOL.h>
#include <std_msgs/String.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Vector3Stamped.h>
#include <sensor_msgs/NavSatFix.h>
#include <geometry_msgs/TransformStamped.h>
#include <sstream>
#include <tf/transform_datatypes.h>
#include <tf/LinearMath/Matrix3x3.h>
#include <tf/LinearMath/Quaternion.h>
#include <math.h>
#define PI 3.14159265

// Global Vars
mavros_msgs::State px4_state;

int offboard_mode_eq;
int manual_mode_eq;
/**
 * This tutorial demonstrates simple receipt of messages over the ROS system.
 */
void posCallback(const geometry_msgs::PoseStamped& msg)
{

  float x_pos = msg.pose.position.x;
  float y_pos = msg.pose.position.y;
  float z_pos = msg.pose.position.z;
  float x_orr = msg.pose.orientation.x;
  float y_orr = msg.pose.orientation.y;
  float z_orr = msg.pose.orientation.z;
  float w_orr = msg.pose.orientation.w;
  //ROS_INFO("z pos: %f, z_orr: %f", z_pos,z_orr);
}


void GPSCallback(const sensor_msgs::NavSatFix& msg)
{

  float lat_pos = msg.latitude;
  float lon_pos = msg.longitude;
  float alt_pos = msg.altitude;

  ROS_INFO("Lat: %f, Lon: %f, Alt: %f", lat_pos,lon_pos,alt_pos);
}

void stateCallback(const mavros_msgs::State& msg)
{

  px4_state = msg;
  offboard_mode_eq = strcmp("OFFBOARD",px4_state.mode.c_str());
  manual_mode_eq = strcmp("MANUAL",px4_state.mode.c_str());

  //ROS_INFO("Con: %d, Arm: %d, Gui: %d, Mode: %s, Truth: %d", px4_state.connected,px4_state.armed,px4_state.guided,px4_state.mode.c_str(),str_eq);
}


int main(int argc, char **argv)
{
  // Ros Init
  ros::init(argc, argv, "px4_flynet_node");
  // Ros Handle
  ros::NodeHandle n;
  
  //======= ROS Publishers ================
  ros::Publisher mavros_pos_control_pub = n.advertise<geometry_msgs::PoseStamped>("/mavros/setpoint_position/local",1000);  
  geometry_msgs::PoseStamped pos_sp;
  
  ros::Publisher mavros_vis_pos_pub = n.advertise<geometry_msgs::PoseStamped>("/mavros/vision_pose/pose",1000);  
  geometry_msgs::PoseStamped vis_pos;
  
  tf::Quaternion quat(0.,0.,0.,1.0);
  
  //======= ROS Subscribers ===============
  // Ros setup async spinners for subscibers
  ros::AsyncSpinner spinner(2);// Do we need 2 threads?
  // Setup Subscribers:
  ros::Subscriber sub_state = n.subscribe("/mavros/state", 1000, stateCallback);
  ros::Subscriber sub_pos = n.subscribe("/mavros/local_position/pose", 1000, posCallback);
  //ros::Subscriber sub_gps = n.subscribe("/mavros/global_position/raw/fix", 1000, GPSCallback);
  // Start the Spinner
  spinner.start();
  
  //==== ROS Clients/Services ==============
  // Mode Set Service
  ros::ServiceClient mavros_set_mode_client = n.serviceClient<mavros_msgs::SetMode>("/mavros/set_mode");
  mavros_msgs::SetMode set_mode;
  set_mode.request.custom_mode = "OFFBOARD";
  
  // Guided Enable Service
  ros::ServiceClient mavros_nav_guided_client = n.serviceClient<mavros_msgs::CommandBool>("/mavros/cmd/guided_enable");
  mavros_msgs::CommandBool nav_guided;
  nav_guided.request.value = true; 
  
  // Arming Service
  ros::ServiceClient mavros_arm_client = n.serviceClient<mavros_msgs::CommandBool>("/mavros/cmd/arming");
  mavros_msgs::CommandBool px4_arm;
  px4_arm.request.value = true; 
  
  
  //Set Home Service 
  ros::ServiceClient mavros_set_home_client = n.serviceClient<mavros_msgs::CommandHome>("/mavros/cmd/set_home");
  mavros_msgs::CommandHome px4_home;
  px4_home.request.current_gps = false;
  px4_home.request.latitude = 40.0021092;
  px4_home.request.longitude = -105.2632344;
  px4_home.request.altitude = 1650; //this needs fixing....
  
  //Takeoff Service
  ros::ServiceClient mavros_takeoff_client = n.serviceClient<mavros_msgs::CommandTOL>("/mavros/cmd/takeoff");
  mavros_msgs::CommandTOL px4_to;
  px4_to.request.min_pitch = 0.15;//rads?
  px4_to.request.yaw = 0.0; // is this right? meh probs...
  px4_to.request.latitude = 40.0021092;
  px4_to.request.longitude = -105.2632344;
  px4_to.request.altitude = 1651.5; //this needs fixing....
  
  //Landing Service 
  ros::ServiceClient mavros_land_client = n.serviceClient<mavros_msgs::CommandTOL>("/mavros/cmd/land");
  mavros_msgs::CommandTOL px4_la;
  px4_la.request.min_pitch = 0.15;//rads?
  px4_la.request.yaw = 0.0; // is this right? meh probs...
  px4_la.request.latitude = 40.0021092;
  px4_la.request.longitude = -105.2632344;
  px4_la.request.altitude = 1650; //this needs fixing....
  
  // Publisher Loop
  ros::Rate loop_rate(100.0);
  int count = 0;
  
  mavros_nav_guided_client.call(nav_guided);
  mavros_set_mode_client.call(set_mode);
  mavros_set_home_client.call(px4_home);
  mavros_arm_client.call(px4_arm);
  mavros_takeoff_client.call(px4_to);
  ros::spinOnce();
  
  //mavros_set_mode_client.call(set_mode);
  //mavros_arm_client.call(px4_arm);
  //ros::spinOnce();
  //
  while (ros::ok()){
<<<<<<< HEAD
    if (!px4_state.armed){
  		mavros_arm_client.call(px4_arm);
    }
    double yaw_sp = 10.0*PI/180.0;
    double qz = sin(yaw_sp/2.0);
    double qw = cos(yaw_sp/2.0);
=======
    
>>>>>>> 58d26ceec9baf104625f58d508aa16ccf63fc18a
    // Write desired setpoint value to pos_sp 
	pos_sp.header.seq = count;
	pos_sp.header.stamp = ros::Time::now();
	pos_sp.header.frame_id = "fcu";
<<<<<<< HEAD
	pos_sp.pose.position.x = 0;
	pos_sp.pose.position.y = 0;
	pos_sp.pose.position.z = 2;
	pos_sp.pose.orientation.x = 0; 
	pos_sp.pose.orientation.y = 0; 
	pos_sp.pose.orientation.z = qz; 
	pos_sp.pose.orientation.w = qw; 
	
=======
	pos_sp.pose.position.x = 0.03576;
	pos_sp.pose.position.y = -1.4754;
	pos_sp.pose.position.z = 1.5;
	pos_sp.pose.orientation.z = 0; 
	//pos_sp.pose.orientation.w = 0; 
>>>>>>> 58d26ceec9baf104625f58d508aa16ccf63fc18a
	
	//double roll_vis = (double)((rand() % 6 +1)-3);
	//double pitch_vis = (double)((rand() % 6 +1)-3);
	//double yaw_vis = (double)((rand() % 6 +1)-3);
	double roll_vis = 0;
	double pitch_vis = 0;
	double yaw_vis = 25*PI/180;
	quat.setEulerZYX(yaw_vis,pitch_vis,roll_vis);
	// Write current position value to vis_pos 
	vis_pos.header.seq = count;
	vis_pos.header.stamp = ros::Time::now();
	vis_pos.header.frame_id = "fcu";
	vis_pos.pose.position.x = (double)((rand() % 10 +1)-5);
	vis_pos.pose.position.y = (double)((rand() % 10 +1)-5);
	vis_pos.pose.position.z = (double)((rand() % 4 +1));
  	vis_pos.pose.orientation.x = (double)quat.x();
	vis_pos.pose.orientation.y = (double)quat.y();
	vis_pos.pose.orientation.z = (double)quat.z();
  	vis_pos.pose.orientation.w = (double)quat.w();
  
    mavros_vis_pos_pub.publish(vis_pos);  
    
	mavros_pos_control_pub.publish(pos_sp);
  
  	// write a check to see if we're in offboard mode
  	
	if ((offboard_mode_eq!=0)&(manual_mode_eq!=0)&(!px4_state.armed)){
		mavros_set_mode_client.call(set_mode);
  		mavros_arm_client.call(px4_arm);
	}
	

	// Spin Once:
    ros::spinOnce();
	// Maintain loop rate
    loop_rate.sleep();

  
    
  }
  
  return 0;
}
