#!/bin/bash
#
# Create workspace at current location and fetch source repositories
#
# License: according to LICENSE.md in the root directory of the PX4 Firmware repository
#set -e
 
WDIR=`pwd`
WORKSPACE=$WDIR/catkin_ws
 
# Setup workspace
mkdir -p $WORKSPACE/src
cd $WORKSPACE/src
catkin_init_workspace
cd $WORKSPACE
catkin_make
sh -c "echo 'source $WORKSPACE/devel/setup.bash' >> ~/.bashrc"
 
