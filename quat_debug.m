clear,clc,close('all')
roll = 0; 
pitch = 0;
yaw = 10;
yaw = yaw*pi/180;
qx = 0;
qy = 0;
qz = sin(yaw/2);
qw = cos(yaw/2);
mag = sqrt(qz^2+qw^2);
qz = qz/mag;
qw = qw/mag;
yaw_c = 2*acos(qw);
yaw_c = yaw_c*180/pi;
yaw_c2 = atan2(2*(qw*qz+qx*qy),1-2*(qy^2+qz^2));
yaw_c2 = yaw_c2*180/pi;
q = [qx;qy;qz;qw];