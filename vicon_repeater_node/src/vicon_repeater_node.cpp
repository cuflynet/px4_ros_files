#include <ros/ros.h>
#include <mavros/mavros.h>
#include <std_msgs/String.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TransformStamped.h>
#include <sstream>

geometry_msgs::PoseStamped vis_pos;
int mocap_count;
int read_flag = 1; 
void viconCallback(const geometry_msgs::TransformStamped& msg)
{
  ++mocap_count;
  float vicon_x = msg.transform.translation.x;
  float vicon_y = msg.transform.translation.y;
  float vicon_z = msg.transform.translation.z;
  float mocap_N = -1*vicon_z;
  float mocap_W = vicon_x;
  float mocap_U = -1*vicon_y;
  vis_pos.header.seq = mocap_count;
  vis_pos.header.stamp = ros::Time::now();
  vis_pos.header.frame_id = "fcu";
  vis_pos.pose.position.x = (double)mocap_N;
  vis_pos.pose.position.y = (double)mocap_W;
  vis_pos.pose.position.z = (double)mocap_U;
  read_flag = 0;
  ROS_INFO("N: %f, W: %f, U: %f", mocap_N,mocap_W,mocap_U);
}

int main(int argc, char **argv)
{
  // Ros Init
  ros::init(argc, argv, "vicon_repeater_node");
  // Ros Handle
  ros::NodeHandle n;
  
  //======= ROS Publishers ================
  
  //ros::Publisher mavros_mocap_pub = n.advertise<geometry_msgs::PoseStamped>("/mavros/mocap/pose",1000);  
  
  ros::Publisher mavros_vis_pos_pub = n.advertise<geometry_msgs::PoseStamped>("/mavros/vision_pose/pose",1000);  
  
  
  //======= ROS Subscribers ===============
  // Ros setup async spinners for subscibers
  ros::AsyncSpinner spinner(1);
  // Setup Subscribers:
  ros::Subscriber sub_vicon = n.subscribe("/wolverine/pose", 1000, viconCallback);
  // Start the Spinner
  spinner.start();
  
  // Publisher Loop

  //
  while (ros::ok()){
  
  	if(read_flag==0){
    	mavros_vis_pos_pub.publish(vis_pos);  
    	ros::spinOnce();
		read_flag = 1; 
	}
	
  }
  
  return 0;
}
